"use strict"

const articleHeader = document.querySelector('.tabs'); 
const articleContent = document.querySelectorAll('.tabs-content li'); 

articleHeader.addEventListener('click',(event)=>{ 

  /*Після завантаження сторінки доступні всі наявні дані */ 

  const articleTitles = articleHeader.querySelectorAll('.tabs-title');

  articleTitles.forEach(e => e.classList.remove('active'));

  event.target.classList.add('active'); 

  articleContent.forEach(e => { 

    e.dataset.content === event.target.dataset.header ? e.style.display='block' : e.style.display='none' 

  });

}); 
